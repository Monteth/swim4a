package com.ct8.monteth.swim4a

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bt1 = findViewById<Button>(R.id.acMain_bt1)
        val bt2 = findViewById<Button>(R.id.acMain_bt2)
        val bt3 = findViewById<Button>(R.id.acMain_bt3)

        bt1.text = "go to activity 4"
        bt1.setOnClickListener {
            startActivity(Intent(applicationContext, Activity4::class.java))
        }

        bt2.text = "go to activity 3"
        bt2.setOnClickListener {
            startActivity(Intent(applicationContext, Activity3::class.java))
        }

        bt3.text = "go to activity 5"
        bt3.setOnClickListener {
            startActivity(Intent(applicationContext, Activity5::class.java))
        }
    }
}
