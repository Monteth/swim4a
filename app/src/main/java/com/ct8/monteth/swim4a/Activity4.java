package com.ct8.monteth.swim4a;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public class Activity4 extends AppCompatActivity {
    Menu _menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        _menu = menu;
        menu.add(R.id.item0, R.id.position_1, 1, "position1");
        menu.add(R.id.item0, R.id.position_2, 2, "position2");
        menu.add(R.id.item0, R.id.position_3, 3, "position3");
        SubMenu subMenu = menu.addSubMenu(R.id.item0, R.id.menu0, Menu.NONE, "Submenu");
        subMenu.add(R.id.item0, R.id.position_0a, 1, "sb1");
        subMenu.add(R.id.item0, R.id.position_0b, 2, "sb2");
        subMenu.add(R.id.item0, R.id.position_0c, 3, "sb3");




        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MenuItem itemSubMenu = _menu.getItem(0);
        MenuItem item1 = _menu.getItem(1);
        MenuItem item2 = _menu.getItem(2);
        MenuItem item3 = _menu.getItem(3);

        MenuItem sbItem0 = itemSubMenu.getSubMenu().getItem(0);
        MenuItem sbItem1 = itemSubMenu.getSubMenu().getItem(1);
        MenuItem sbItem2 = itemSubMenu.getSubMenu().getItem(2);

        switch (item.getItemId()) {
            case R.id.position_1:
                item1.setTitle("Title from pos1");
                item2.setTitle("Title from pos1");
                item3.setTitle("Title from pos1");
                break;

            case R.id.position_2:
                item1.setTitle("Title from pos2");
                item2.setTitle("Title from pos2");
                item3.setTitle("Title from pos2");
                break;
            case R.id.position_3:
                item1.setTitle("Title from pos3");
                item2.setTitle("Title from pos3");
                item3.setTitle("Title from pos3");
                break;
            case R.id.position_0a:
                sbItem0.setCheckable(true);
                break;
            case R.id.position_0b:
                sbItem1.setCheckable(true);
                break;
            case R.id.position_0c:
                sbItem2.setCheckable(true);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
