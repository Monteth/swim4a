package com.ct8.monteth.swim4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.SubMenu;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.id.group1, R.id.g1_i1, 1, "customOption");
        SubMenu subMenu = menu.addSubMenu(R.id.item0, R.id.menu0, Menu.NONE, "SubMenu 1");
        subMenu.add(R.id.item0, R.id.position_1, 1, "item1");
        subMenu.add(R.id.item0, R.id.position_2, 2, "item2");

        subMenu = menu.addSubMenu(R.id.item0, R.id.menu0, Menu.NONE, "SubMenu 2");
        subMenu.add(R.id.group1, R.id.g1_i1, 1, "item1 from g1");
        subMenu.add(R.id.group1, R.id.g1_i2, 2, "item2 from g1");
        subMenu.add(R.id.group1, R.id.g1_i3, 3, "item3 from g1");
        subMenu.getItem().getSubMenu().getItem(1).setIcon(R.drawable.ic_launcher_foreground);

        return true;
    }
}
