package com.ct8.monteth.swim4a

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.widget.Toast
import android.view.MenuItem


class Activity1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)
        setSupportActionBar(findViewById(R.id.ac1_toolbar))
    }

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu1, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_i1 -> Toast.makeText(this, "Icon clicked", Toast.LENGTH_SHORT).show()

            R.id.menu_i2 -> Toast.makeText(this, "item1 clicked", Toast.LENGTH_SHORT).show()

            R.id.menu_i3 -> if (item.isChecked) {
                item.isChecked = false
                } else {
                item.isChecked = true
                    Toast.makeText(this, "item2 checked", Toast.LENGTH_SHORT).show()
                }
        }

        return true
    }
}
