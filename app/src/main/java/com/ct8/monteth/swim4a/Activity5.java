package com.ct8.monteth.swim4a;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Activity5 extends AppCompatActivity {


    Button button1;
    Button button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);

        button1 = findViewById(R.id.ac5_bt1);
        button2 = findViewById(R.id.ac5_bt2);
        registerForContextMenu(button1);
        registerForContextMenu(button2);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        switch (v.getId()){
            case R.id.ac5_bt1:
                menu.add(R.id.item0, R.id.position_1, 1, "opt1");
                menu.add(R.id.item0, R.id.position_2, 2, "opt2");

                break;
            case R.id.ac5_bt2:
                menu.add(R.id.item0, R.id.position_3, 1, "opt1");
                menu.add(R.id.item0, R.id.position_0a, 2, "opt2");

                break;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.position_1:
                button1.setBackgroundColor(Color.BLUE);
                break;
            case R.id.position_2:
                button1.setBackgroundColor(Color.RED);
                break;
            case R.id.position_3:
                button2.setTextSize(15);
                break;
            case R.id.position_0a:
                button2.setTextSize(30);
                break;
        }
        return true;
    }
}
